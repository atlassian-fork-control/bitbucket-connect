# <%= name %>

<%= description %>

## Development

```
npm install
npm run migrate
CONNECT_BASE_URL=<base url> npm run start
```

## Running tests

```
npm run test
```
