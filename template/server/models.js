import bookshelf from './bookshelf';

export const Connection = bookshelf.Model.extend({
  tableName: 'connections',
  idAttribute: 'clientKey',
});
