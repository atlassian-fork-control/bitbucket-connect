import isPlainObject from 'lodash.isplainobject';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

export function webpackify(app, webpackConfig) {
  const HOT_CLIENT = 'webpack-hot-middleware/client';
  const config = Object.assign({}, webpackConfig);

  if (isPlainObject(config.entry)) {
    config.entry = Object.keys(config.entry).forEach(key => {
      if (Array.isArray(config.entry[key])) {
        config.entry[key].push(HOT_CLIENT);
      } else {
        config.entry[key] = [config.entry[key], HOT_CLIENT];
      }
    });
  } else if (!Array.isArray(config.entry)) {
    config.entry = [config.entry, HOT_CLIENT];
  } else {
    config.entry.push(HOT_CLIENT);
  }


  if (!Array.isArray(config.plugins)) {
    config.plugins = [];
  }

  config.plugins.push(
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  );

  const compiler = webpack(config);

  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
  }));

  app.use(webpackHotMiddleware(compiler, { path: '/__webpack_hmr' }));
}
