import omit from 'lodash.omit';

import { Connection } from './models';

function stringifyLifecyclePayload(payload) {
  return Object.assign({}, payload, {
    user: JSON.stringify(payload.user),
    principal: JSON.stringify(payload.principal),
    consumer: JSON.stringify(payload.consumer),
  });
}

export function installed(req, res) {
  const { clientKey } = req.body;
  const connection = omit(stringifyLifecyclePayload(req.body), 'eventType');

  Connection.forge({ clientKey }).destroy().then(() => {
    Connection.forge(connection).save(null, { method: 'insert' }).then(() => {
      res.status(204).send();
    }).catch(() => {
      res.status(500).send('Unable to save connection');
    });
  });
}

export function uninstalled(req, res) {
  const { clientKey } = req.body;

  Connection.forge({ clientKey }).destroy().then(() => {
    res.status(204).send();
  }).catch(() => {
    res.status(500).send('Unable to clean up connection');
  });
}
