import { ExpressAddon, Addon } from 'bitbucket-connect';

import { installed, uninstalled } from './routes';
import { validateJwtToken, exposeBaseUrl, attachHttpClientFactory } from './middleware';

const addon = new ExpressAddon({
  key: <%= quotedKey %>,
  name: <%= quotedName %>,
  description: <%= quotedDescription %>,
  vendor: {
    name: <%= quotedVendorName %>,
    url: <%= quotedVendorUrl %>,
  },
  baseUrl: process.env.CONNECT_BASE_URL,
}, {
  middleware: [
    validateJwtToken,
    exposeBaseUrl,
    attachHttpClientFactory,
  ],
});

addon.registerContexts(<%= contexts %>);
addon.registerScopes(<%= scopes %>);

if (process.env.CONNECT_OAUTH_CONSUMER_KEY) {
  addon.registerOauthConsumer({ clientId: process.env.CONNECT_OAUTH_CONSUMER_KEY });
}

addon.registerLifecycleRoute('/installed', Addon.Lifecycles.INSTALLED, installed);
addon.registerLifecycleRoute('/uninstalled', Addon.Lifecycles.UNINSTALLED, uninstalled);

export default addon;
