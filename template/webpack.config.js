var webpack = require('webpack');
var path = require('path');

module.exports = {
  context: path.join(__dirname, 'frontend'),
  entry: [
    './index.js',
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/static',
    filename: 'bundle.js',
  },
  devtool: 'eval-source-map',
  resolve: {
    extensions: [
      '',
      '.js',
    ],
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel'
      },
    ],
  },
};
