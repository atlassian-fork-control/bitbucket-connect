/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
import expect from 'expect';

import Addon from '../src/addon';

describe('Addon', function () {
  it('should create an instance with default descriptor', function () {
    const addon = new Addon();
    expect(addon.descriptor).toEqual({});
  });

  it('should create an instance with the passed descriptor props', function () {
    const props = { key: 'test-add-on' };
    const addon = new Addon(props);
    expect(addon.descriptor).toEqual(props);
  });

  it('should create an instance with default logger', function () {
    const addon = new Addon();
    expect(addon.logger).toEqual(console.log); // eslint-disable-line no-console
  });

  it('should create an instance with the passed logger fn', function () {
    const logger = msg => msg;
    const addon = new Addon(null, { logger });
    expect(addon.logger).toEqual(logger);
  });

  describe('#log', function () {
    it('should call logger function with message', function () {
      const spy = expect.createSpy();
      const addon = new Addon(null, { logger: spy });

      addon.log('test log message');
      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith('test log message');
    });
  });

  describe('#get', function () {
    it('should get key from descriptor object', function () {
      const addon = new Addon({ key: 'test-add-on' });
      expect(addon.get('key')).toEqual('test-add-on');
    });
  });

  describe('#set', function () {
    it('should set key on descriptor object', function () {
      const addon = new Addon();
      addon.set('key', 'test-add-on');
      expect(addon.descriptor.key).toEqual('test-add-on');
    });

    it('should override value if it exists', function () {
      const addon = new Addon();
      addon.set('key', 'add-on-key');
      addon.set('key', 'new-add-on-key');
      expect(addon.descriptor.key).toEqual('new-add-on-key');
    });
  });

  describe('#registerContexts', function () {
    it('should add contexts to descriptor object', function () {
      const addon = new Addon();
      addon.registerContexts(['account']);
      expect(addon.descriptor.contexts).toEqual(['account']);
    });

    it('should add contexts to existing array if it exists', function () {
      const addon = new Addon();
      addon.registerContexts(['personal']);
      addon.registerContexts(['account']);
      expect(addon.descriptor.contexts).toEqual(['personal', 'account']);
    });

    it('should warn when the context is unsupported', function () {
      const addon = new Addon();
      const spy = expect.spyOn(addon, 'log');
      addon.registerContexts(['some-unsupported-context']);
      expect(spy.calls[0].arguments[0]).toInclude('not supported');
    });
  });

  describe('#registerScopes', function () {
    it('should add scopes to descriptor object', function () {
      const addon = new Addon();
      addon.registerScopes(['account']);
      expect(addon.descriptor.scopes).toEqual(['account']);
    });

    it('should add scopes to existing array if it exists', function () {
      const addon = new Addon();
      addon.registerScopes(['repository']);
      addon.registerScopes(['account']);
      expect(addon.descriptor.scopes).toEqual(['repository', 'account']);
    });

    it('should warn when the scope is unsupported', function () {
      const addon = new Addon();
      const spy = expect.spyOn(addon, 'log');
      addon.registerScopes(['some-unsupported-scope']);
      expect(spy.calls[0].arguments[0]).toInclude('not supported');
    });
  });

  describe('#registerLifecycle', function () {
    it('should add lifecycle to descriptor object', function () {
      const addon = new Addon();
      addon.registerLifecycle('installed', '/connect/installed');
      expect(addon.descriptor.lifecycle).toEqual({ installed: '/connect/installed' });
    });

    it('should add lifecycle to existing object if it exists', function () {
      const addon = new Addon();
      addon.registerLifecycle('installed', '/connect/installed');
      addon.registerLifecycle('uninstalled', '/connect/uninstalled');
      expect(addon.descriptor.lifecycle).toEqual({
        installed: '/connect/installed',
        uninstalled: '/connect/uninstalled',
      });
    });

    it('should warn when the lifecycle is unsupported', function () {
      const addon = new Addon();
      const spy = expect.spyOn(addon, 'log');
      addon.registerLifecycle('some-unsupported-lifecycle', '/dev/null');
      expect(spy.calls[0].arguments[0]).toInclude('not supported');
    });

    it('should override lifecycle value if it exists', function () {
      const addon = new Addon();
      addon.registerLifecycle('installed', '/connect/installed');
      addon.registerLifecycle('installed', '/installed');
      expect(addon.descriptor.lifecycle.installed).toEqual('/installed');
    });
  });

  describe('#registerModule', function () {
    it('should add module to descriptor object', function () {
      const addon = new Addon();
      addon.registerModule('webItems', { location: 'org.bitbucket.repository.actions' });
      expect(addon.descriptor.modules).toEqual({ webItems: [{ location: 'org.bitbucket.repository.actions' }] });
    });

    it('should add module to existing array if it exists', function () {
      const addon = new Addon();
      addon.registerModule('webItems', { location: 'org.bitbucket.repository.actions' });
      addon.registerModule('webItems', { location: 'org.bitbucket.repository.navigation' });
      expect(addon.descriptor.modules.webItems).toEqual([
        { location: 'org.bitbucket.repository.actions' },
        { location: 'org.bitbucket.repository.navigation' },
      ]);
    });

    it('should warn when the module type is unsupported', function () {
      const addon = new Addon();
      const spy = expect.spyOn(addon, 'log');
      addon.registerModule('some-unsupported-type', { location: 'org.bitbucket.repository.actions' });
      expect(spy.calls[0].arguments[0]).toInclude('not supported');
    });

    it('should not allow multiple oauthConsumer modules to be added', function () {
      const addon = new Addon();
      addon.registerModule('oauthConsumer', { clientId: 'some-client-key' });
      addon.registerModule('oauthConsumer', { clientId: 'some-other-key' });
      expect(Array.isArray(addon.descriptor.modules.oauthConsumer)).toEqual(false);
    });

    it('should override oauthConsumer value if it exists', function () {
      const addon = new Addon();
      addon.registerModule('oauthConsumer', { clientId: 'some-client-key' });
      addon.registerModule('oauthConsumer', { clientId: 'some-other-key' });
      expect(addon.descriptor.modules.oauthConsumer.clientId).toEqual('some-other-key');
    });

    it('should allow adding of different types of modules', function () {
      const addon = new Addon();
      addon.registerModule('webItems', { location: 'org.bitbucket.repository.actions' });
      addon.registerModule('oauthConsumer', { clientId: 'some-client-key' });
      addon.registerModule('configurePage', { name: { value: 'Configuration' } });
      expect(addon.descriptor.modules).toEqual({
        webItems: [{ location: 'org.bitbucket.repository.actions' }],
        oauthConsumer: { clientId: 'some-client-key' },
        configurePage: [{ name: { value: 'Configuration' } }],
      });
    });
  });

  describe('#registerAdminPage', function () {
    it('should add adminPage to descriptor object', function () {
      const addon = new Addon();
      addon.registerAdminPage({ location: 'org.bitbucket.account.admin' });
      expect(addon.descriptor.modules.adminPages).toEqual([{ location: 'org.bitbucket.account.admin' }]);
    });

    it('should warn when the location is unsupported', function () {
      const addon = new Addon();
      const spy = expect.spyOn(addon, 'log');
      addon.registerAdminPage({ location: 'some.location.that.doesnt.exist' });
      expect(spy.calls[0].arguments[0]).toInclude('not supported');
    });
  });

  describe('#registerConfigurePage', function () {
    it('should add configurePage to descriptor object', function () {
      const addon = new Addon();
      addon.registerConfigurePage({ name: { value: 'Configuration' } });
      expect(addon.descriptor.modules.configurePage).toEqual([{ name: { value: 'Configuration' } }]);
    });
  });

  describe('#registerFileViews', function () {
    it('should add fileViews to descriptor object', function () {
      const addon = new Addon();
      addon.registerFileViews({ name: { value: 'File viewer' } });
      expect(addon.descriptor.modules.fileViews).toEqual([{ name: { value: 'File viewer' } }]);
    });
  });

  describe('#registerOauthConsumer', function () {
    it('should add oauthConsumer to descriptor object', function () {
      const addon = new Addon();
      addon.registerOauthConsumer({ clientId: 'some-client-key' });
      expect(addon.descriptor.modules.oauthConsumer.clientId).toEqual('some-client-key');
    });
  });

  describe('#registerProfileTab', function () {
    it('should add profileTab to descriptor object', function () {
      const addon = new Addon();
      addon.registerProfileTab({ name: { value: 'Profile' } });
      expect(addon.descriptor.modules.profileTabs).toEqual([{ name: { value: 'Profile' } }]);
    });
  });

  describe('#registerRepoPage', function () {
    it('should add repoPage to descriptor object', function () {
      const addon = new Addon();
      addon.registerRepoPage({ location: 'org.bitbucket.repository.navigation' });
      expect(addon.descriptor.modules.repoPages).toEqual([{ location: 'org.bitbucket.repository.navigation' }]);
    });

    it('should warn when the location is unsupported', function () {
      const addon = new Addon();
      const spy = expect.spyOn(addon, 'log');
      addon.registerRepoPage({ location: 'some.location.that.doesnt.exist' });
      expect(spy.calls[0].arguments[0]).toInclude('not supported');
    });
  });

  describe('#registerWebItem', function () {
    it('should add webItem to descriptor object', function () {
      const addon = new Addon();
      addon.registerWebItem({ location: 'org.bitbucket.repository.navigation' });
      expect(addon.descriptor.modules.webItems).toEqual([{ location: 'org.bitbucket.repository.navigation' }]);
    });

    it('should warn when the location is unsupported', function () {
      const addon = new Addon();
      const spy = expect.spyOn(addon, 'log');
      addon.registerWebItem({ location: 'some.location.that.doesnt.exist' });
      expect(spy.calls[0].arguments[0]).toInclude('not supported');
    });
  });

  describe('#registerWebPanel', function () {
    it('should add webPanel to descriptor object', function () {
      const addon = new Addon();
      addon.registerWebPanel({ location: 'org.bitbucket.repository.overview.informationPanel',
                               key: 'my-test-web-panel' });
      expect(addon.descriptor.modules.webPanels).toEqual([{ location: 'org.bitbucket.repository.overview.informationPanel',
                                                           key: 'my-test-web-panel',
                                                           name: { value: 'my-test-web-panel' } }]);
    });

    it('should warn when the location is unsupported', function () {
      const addon = new Addon();
      const spy = expect.spyOn(addon, 'log');
      addon.registerWebPanel({ location: 'some.location.that.doesnt.exist' });
      expect(spy.calls[0].arguments[0]).toInclude('not supported');
    });
  });

  describe('#registerWebhooks', function () {
    it('should add webhooks to descriptor object', function () {
      const addon = new Addon();
      addon.registerWebhooks({ event: '*', url: '/connect/hooks' });
      expect(addon.descriptor.modules.webhooks).toEqual([{ event: '*', url: '/connect/hooks' }]);
    });
  });
});
