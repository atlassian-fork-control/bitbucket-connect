import jwt from 'atlassian-jwt';
import moment from 'moment';
import axios from 'axios';
import { parse as urlParse } from 'url';

/** Class representing a HTTP client for connecting to the Bitbucket API. */
class HttpClient {

  /**
   * @param {Addon} addon - The addon, used to get it's key from the
                            descriptor for creating the JWT token.
   * @param {String} clientKey - The clientKey, distinguishes each instance
                                 of the addon installed on Bitbucket
   * @param {String} sharedSecret - associated with the clientKey
                                    used to verify the JWT token
   * @param {String} baseUrl - typically https://api.bitbucket.org/
   */
  constructor(addon, { clientKey, sharedSecret, baseUrl }) {
    if (!clientKey) throw new Error('clientKey is required');
    if (!sharedSecret) throw new Error('sharedSecret is required');
    if (!baseUrl) throw new Error('baseUrl is required');
    this.addon = addon;
    this.clientKey = clientKey;
    this.sharedSecret = sharedSecret;
    this.client = axios.create({ baseURL: baseUrl });
  }

  /**
   * @param {String} url - The url to get, should start with '/api/'
   * @return {Promise} a promise that returns a response if resolved, or an
   * error if rejected.
   *
   * The response schema is described at
   * https://github.com/mzabriskie/axios#response-schema
   *
   * Handling errors is documented at
   * https://github.com/mzabriskie/axios#handling-errors
   * but that might actually be misleading, so use caution and test your code.
   */
  get(url) {
    if (!url) throw new Error('url is required');
    const jwtToken = this._createJwtToken(url, 'GET');
    return this.client.get(url, { headers: { Authorization: `JWT ${jwtToken}` } });
  }

  _createJwtToken(url, method) {
    const now = moment().utc();
    const urlObject = urlParse(url, true);
    const qshReq = { method, path: urlObject.pathname, query: urlObject.query };
    const payload = {
      exp: now.add(3, 'minutes').unix(),
      iat: now.unix(),
      iss: this.addon.get('key'),
      qsh: jwt.createQueryStringHash(qshReq),
      sub: this.clientKey,
    };
    return jwt.encode(payload, this.sharedSecret, 'HS256');
  }

}

export default HttpClient;
